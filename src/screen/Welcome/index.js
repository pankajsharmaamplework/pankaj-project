import React from "react";
import { SafeAreaView, View, Text, TouchableOpacity, StyleSheet, Image, Button } from "react-native";
import { s, vs, ms } from 'react-native-size-matters';
import {Headertext,Plantext,CustomButton} from "@components/index";
import { COLORS } from "@theme";
const Welcome = ({ navigation }) => {
 return (
  <SafeAreaView style={styles.safeview}>
    <View style={styles.imgbox}>
     <Image
      style={styles.image}
      source={require('@assets/images/Icon.png')}
     />
    </View>
    <Headertext
     value={"Welcome to \nPhoneshake"}
     style={{
      fontSize:ms(30),
     }}
    />
    <View style={styles.button}>
     <CustomButton
     title="Get Started"
     onPress={() => navigation.navigate('LoginWithPhone')}
     />
    </View>
    <View style={styles.plantext}>
    <Plantext
     value="Phoneshake for businesses"
    />
    </View>

  </SafeAreaView>
 );
}
const styles = StyleSheet.create({
 safeview: {
  flex: 1,
  backgroundColor:COLORS.white,
 },
 imgbox:{
  marginTop: vs(140),
 },
 image: {
  height: vs(120),
  width: s(120),
  alignSelf: "center",
 },
 button:{
  marginTop: vs(200),
 },
 plantext:{
  marginTop: vs(18),
 },

});
export default Welcome;