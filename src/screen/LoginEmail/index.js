import React, { useState } from "react";
import { SafeAreaView, View, Text, TextInput, TouchableWithoutFeedback, Keyboard, StyleSheet, Button, TouchableOpacity } from "react-native";
import { s, vs, ms } from 'react-native-size-matters';
import { Headertext, Plantext, CustomButton, Hint, InputText } from "@components/index";
import {COLORS} from "@theme";
const LoginEmail = ({ navigation }) => {
 const [email, setEmail] = useState("")

 return (
  <SafeAreaView style={styles.safeview}>
   <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    <View style={styles.view}>
     <View style={styles.headerText}>
      <Headertext
       value={"What's your email?"}
      />
     </View>
     <TouchableOpacity
      onPress={() => navigation.navigate("LoginWithPhone")}
     >
      <Plantext
       value="Use phone instead"
      />
     </TouchableOpacity>
     <View style={styles.inputview}>
      <InputText
       placeholder="Email Address"
       textContentType="emailAddress"
       autoCapitalize="none"
       value={email}
       onChangeText={(value) => setEmail(value)}
      />
     </View>
     <View style={styles.hintText}>
      <Hint
       value="We'll send you an SMS verification code"
      />
     </View>
      <CustomButton
       title="Continue"
       disabled={email.length > 0 ? false : true}
       onPress={() => navigation.navigate("verification")}
      />
    </View>
   </TouchableWithoutFeedback>
  </SafeAreaView>
 );
}
const styles = StyleSheet.create({
 safeview: {
  flex: 1,
  backgroundColor: COLORS.white,
 },
 headerText: {
  marginTop: vs(54),
  marginBottom:vs(20),
 },
 inputview: {
  flexDirection: "row",
  marginTop: vs(70),
  marginLeft: s(30),
  marginRight: s(25),
 },
 hintText:{
  marginTop:vs(-15),
  marginBottom:vs(100)
 },
});
export default LoginEmail;