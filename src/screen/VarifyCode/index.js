import React, { useState } from "react";
import { SafeAreaView, View, Text, TouchableOpacity, KeyboardAvoidingView, StyleSheet } from "react-native";
import { s, vs, ms } from 'react-native-size-matters';
import { TextInputMask } from 'react-native-masked-text';
import { Headertext, Plantext, CustomButton, Hint, InputText } from "@components/index";
import {COLORS} from "@theme";
const OTP = Math.floor(100000 + Math.random() * 900000);
const VerifyCode = ({ navigation }) => {
 const [text, setText] = useState("");
 const [wrong, setWrong] = useState(false);
 const otpcheck = () => {
  if (text == OTP) {
   navigation.navigate('Username')
  } else {
   setWrong(true);
   setText("")
  }
 }
 console.log(OTP);
 return (
  <SafeAreaView style={styles.safeview}>
    <View style={styles.headerText}>
     <Headertext
      value={"Please enter \nthe confirmation code"}
     />
    </View>
    <TouchableOpacity>
     <Plantext
       value="Did not receive code"
     />
    </TouchableOpacity>
    <View style={styles.inputview}>
     <TextInputMask
      type={'custom'}
      options={{
       mask: '999-999'
      }}
      value={text}
      onChangeText={(text) => { setText(text.replace('-', '')); }}
      keyboardType="numeric"
      placeholder="Enter code"
      style={{ fontSize: 18, marginBottom: 5, marginLeft: 80 }}
     />
    </View>
    {wrong ?
    <Hint
     value="The code entered is incorrect"
     style={{
      color: COLORS.errColor,
      marginTop: vs(5),
     }}
    />
     :
     <Hint
     value="Enter the confirmation code sent to you"
     style={{
      color: COLORS.hintColor,
      marginTop: vs(5),
     }}
    />
    }
    <KeyboardAvoidingView>
     <View style={styles.button}>
      <CustomButton
       title="Continue"
       disabled={text.length > 0 ? false : true}
       onPress={otpcheck}
      />
     </View>
    </KeyboardAvoidingView>
  </SafeAreaView>
 );
}
const styles = StyleSheet.create({
 safeview: {
  flex: 1,
  backgroundColor: COLORS.white,
 },
 headerText: {
  marginTop: vs(74),
  marginBottom: vs(20),
 },
 inputview: {
  flexDirection: "row",
  marginTop: vs(78),
  marginLeft: s(60),
  marginRight: s(45),
  borderBottomWidth: 1,
  borderColor: COLORS.hintColor,
 },
 button: {
  marginTop: vs(85),
 },
});
export default VerifyCode;