import React, { useState } from "react";
import { SafeAreaView, View, Text, StyleSheet } from "react-native";
import { s, vs, ms } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Headertext, Plantext, CustomButton, Hint, InputText } from "@components/index";
import {COLORS} from "@theme";
const Handle = ({ navigation }) => {
 const [handle, setHandle] = useState("")
 let str;
 if(handle.charAt(0)=="@")
   {
     str=handle.replace('@',"@");
   }
  else if(handle.length==0){
    str=handle;
  }
  else{
    str="@"+handle;
  }
 return (            
  <SafeAreaView style={styles.safeview}>
   <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
    <Icon name="angle-left" size={35} color={COLORS.backIcon} style={styles.icon} onPress={() => navigation.goBack()} />
   </View>
   <View style={styles.headerText}>
    <Headertext
     value={"Choose a @Handle,\nyour unique name for sharing\nyour contact with anyone"}
    />
    </View>
    <View style={styles.inputview}>
     <InputText
      placeholder="@Handle"
      value={str}
      onChangeText={(value) => setHandle(value)}

     />
    </View>
    <View style={styles.hintText}>
      <Hint
       value={"phoneshake.me/"+str}
      />
     </View>
     <CustomButton
      title="Continue"
      disabled={handle.length > 0 ? false : true}
      onPress={() => navigation.navigate("Privacy")}
     />
     
  </SafeAreaView>
 );
}
const styles = StyleSheet.create({
 safeview: {
  flex: 1,
  backgroundColor: COLORS.white,
 },
 icon: {
  marginLeft: s(10)
 },
 headerText:{
  marginTop: vs(30),
 },
 inputview: {
  flexDirection: "row",
  marginTop: vs(78),
  marginLeft: s(30),
  marginRight: s(30),
 },
 hintText:{
  marginTop:vs(-15),
  marginBottom:vs(100)
 },
});
export default Handle;