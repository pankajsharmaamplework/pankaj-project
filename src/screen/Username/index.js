import React, { useState } from "react";
import { SafeAreaView, View, Text, KeyboardAvoidingView, StyleSheet } from "react-native";
import { s, vs, ms } from 'react-native-size-matters';
import { Headertext, Plantext, CustomButton, Hint, InputText } from "@components/index";
import {COLORS} from "@theme";
const Username = ({ navigation }) => {
 const [name, setName] = useState("")
 return (
  <SafeAreaView style={styles.safeview}>
    <View style={styles.headerText}>
     <Headertext
      value={"What’s your name?"}
     />
    </View>
    <View style={styles.inputview}>
     <InputText
      placeholder="Full Name"
      value={name}
      onChangeText={(value) => setName(value)}
     />
    </View>
    <View style={styles.hintText}>
      <Hint
       value="We'll send you an SMS verification code"
      />
     </View>
    <KeyboardAvoidingView>
      <CustomButton
       title="Continue"
       disabled={name.length > 0 ? false : true}
       onPress={() => navigation.navigate("Onboard")}
      />
    </KeyboardAvoidingView>
  </SafeAreaView>
 );
}
const styles = StyleSheet.create({
 safeview: {
  flex: 1,
  backgroundColor: COLORS.white,
 },
 headerText:{
  marginTop: vs(74),
 },
 inputview: {
  flexDirection: "row",
  marginTop: vs(78),
  marginLeft: s(30),
  marginRight: s(30),
 },
 hintText:{
  marginTop:vs(-15),
  marginBottom:vs(100)
 },
});
export default Username;