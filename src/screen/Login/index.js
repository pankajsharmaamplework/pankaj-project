import React, { useState } from "react";
import { TouchableWithoutFeedback, Keyboard, TouchableOpacity, SafeAreaView, View, KeyboardAvoidingView, StyleSheet, Alert} from "react-native";
import { s, vs, ms } from 'react-native-size-matters';
import CountryPicker from 'react-native-country-picker-modal';
import { TextInputMask } from 'react-native-masked-text';
import {Headertext,Plantext,CustomButton,Hint} from "@components/index";
import {COLORS} from "@theme";
const Login = ({ navigation }) => {
const [country, setCountry] = useState(null)
const [countryCode, setCountryCode] = useState('US')
const [phoneNumber, setphoneNumber] = useState("")
const verificationCode = () => {
  Alert.alert(
   'Number is Correct?',
   (country) ?
    `Sending a confirmation code to\n+ ${country.callingCode} - ${phoneNumber}` : `Sending a confirmation code to\n+ ${+1} - ${phoneNumber}`,
   [
    {
     text: 'Cancel',
     onPress: () => { },
     style: 'cancel'
    },
    {
     text: 'Yes',
     onPress: () => navigation.navigate("verification"),
    }
   ]
  )
 }
 const OnSelect = (country) => {
  setCountryCode(country.cca2)
  setCountry(country)
 }
 return (
  <TouchableWithoutFeedback onPress={() => {
   Keyboard.dismiss();
  }}>
   <SafeAreaView style={styles.safeview}>
    <View style={styles.view}>
        <Headertext
            value={"What's your \nmobile number?"}
        />
    </View>
     <TouchableOpacity
      onPress={() => navigation.navigate("LoginWithEmail")}
     >
        <Plantext
            value="Use email instead"
        />
     </TouchableOpacity>
     <View style={styles.inputview}>
      {country !== null ? (
            <Plantext 
            value={country.cca2}
            style={{
                fontSize:ms(18),
                marginLeft:s(25),
                marginTop:vs(2)
            }}
            />
      )
       :
            <Plantext 
            value="US"
            style={{
                fontSize:ms(18),
                marginLeft:s(25),
                marginTop:vs(2)
            }}
            />
      }
      <CountryPicker
       onSelect={OnSelect}
       countryCode={countryCode}
       withFlagButton={false}
       withCallingCodeButton={true}
       theme={{ onBackgroundTextColor: COLORS.buttonColor, fontSize: 18 }}
      />
      <TextInputMask
       type={'custom'}
       options={{
        mask: '999-999-9999'
       }}
       value={phoneNumber}
       onChangeText={(text) => {
        setphoneNumber(text);
       }}
       keyboardType="numeric"
       placeholder="Mobile Number"
       style={{ fontSize: 18, marginBottom: 5, marginLeft: 10 }}
      />
     </View>
     <View style={styles.hintText}>
        <Hint
            value="We'll send you an SMS verification code"
        />
     </View>
     <KeyboardAvoidingView>
 
       <CustomButton
        title="Continue"
        disabled={phoneNumber.length > 0 ? false : true}
        onPress={verificationCode}
       />
     </KeyboardAvoidingView>
   </SafeAreaView>
  </TouchableWithoutFeedback>
 );
}
const styles = StyleSheet.create({
 safeview: {
  flex: 1,
  backgroundColor: COLORS.white,
 },
 view:{
     marginTop:vs(54),
     marginBottom:vs(20),
 },
 inputview: {
  flexDirection: "row",
  marginTop: vs(78),
  marginLeft: s(60),
  marginRight: s(45),
  borderBottomWidth: 1,
  borderColor: COLORS.hintColor,
 },
 hintText:{
    marginTop:vs(7),
    marginBottom:vs(100),
 },
});
export default Login;