import React, { useState } from "react";
import { SafeAreaView, View, TouchableOpacity, StyleSheet } from "react-native";
import { s, vs, ms } from 'react-native-size-matters';
import { Headertext, Plantext, CustomButton, Hint, InputText } from "@components/index";
import Icon from 'react-native-vector-icons/FontAwesome';
import {COLORS} from "@theme";
const Position = ({ navigation }) => {
 const [position, setPosotion] = useState("")
 return (
  <SafeAreaView style={styles.safeview}>
   <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
    <Icon name="angle-left" size={35} color={COLORS.backIcon} style={styles.icon} onPress={() => navigation.goBack()} />
    <TouchableOpacity
     onPress={() => navigation.navigate("Handle")}
    >
      <Plantext value="Skip" style={{fontSize:ms(17), marginRight:s(10)}}/>
    </TouchableOpacity>
   </View>
   <View style={styles.headerText}>
    <Headertext
     value={"What’s your \nposition/designation?"}
    />
    <View style={styles.inputview}>
     <InputText
      placeholder="Position"
      value={position}
      onChangeText={(value) => setPosotion(value)}
     />
    </View>
    <View style={styles.hintText}>
      <Hint
       value="We'll send you an SMS verification code"
      />
     </View>
     <CustomButton
      title="Continue"
      disabled={position.length > 0 ? false : true}
      onPress={() => navigation.navigate("Handle")}
     />
   </View>
  </SafeAreaView>
 );
}
const styles = StyleSheet.create({
 safeview: {
  flex: 1,
  backgroundColor: COLORS.white,
 },
 icon: {
  marginLeft: s(10)
 },
 headerText:{
  marginTop: vs(50),
 },
 inputview: {
  flexDirection: "row",
  marginTop: vs(65),
  marginLeft: s(30),
  marginRight: s(30),
 },
 hintText:{
  marginTop:vs(-15),
  marginBottom:vs(100)
 },
});
export default Position;