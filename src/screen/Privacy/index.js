import React, { useState } from "react";
import { SafeAreaView, View, Text, StyleSheet,ScrollView } from "react-native";
import { s, vs, ms } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome';
import { CheckBox } from 'react-native-elements'
import { Headertext, Plantext, CustomButton, Hint, InputText } from "@components/index";
import {COLORS} from "@theme";
const Privacy = ({ navigation }) => {

 const [isSelected, setSelection] = useState(false);
 return (
  <SafeAreaView style={styles.safeview}>
   <View style={{ flexDirection: "row" }}>
    <Icon name="angle-left" size={35} color={COLORS.backIcon} style={styles.icon} onPress={() => navigation.goBack()} />
    <Text style={styles.head}>Privacy Policy</Text>
   </View>
   <ScrollView style={styles.data}>
    <Text style={styles.title}>1. First Clause</Text>
    <Text style={styles.content}>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of “de Finibus Bonorum et Malorum” (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..”, comes from a line in section 1.10.32.'</Text>
    <Text style={styles.title}>2. Second Clause</Text>
    <Text style={styles.content}>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of “de Finibus Bonorum et Malorum” (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..”, comes from a line in section 1.10.32.'</Text>
   </ScrollView>
   <View style={styles.footer}>
     <View style={styles.buttomView}>
      <CheckBox
       checked={isSelected ? true : false}
       onPress={() => setSelection(!isSelected)}
      />
      <Text style={styles.term}>Accept <Text style={{ color: COLORS.buttonColorOpacity }}>Privacy Policy </Text>and <Text style={{ color: 'rgba(32,142,251,0.5)' }}>Terms of Service</Text></Text>
     </View>
     <CustomButton
      title="Complete"
      disabled={isSelected === true ? false : true}
     />
   </View>
  </SafeAreaView>
 );
}
const styles = StyleSheet.create({
 safeview: {
  flex: 1,
  backgroundColor:COLORS.white,
 },
 icon: {
  marginLeft: s(10)
 },
 head: {
  fontSize: 20,
  lineHeight: vs(24),
  alignSelf: "center",
  marginLeft: s(95)
 },
 data: {
  margin: s(16),
 },
 title:{
  fontSize:12,
  lineHeight:vs(14),
  textAlign:"left",
  letterSpacing:0.04,
  marginBottom:vs(8)
 },
 content:{
  fontSize:14,
  lineHeight:vs(16),
  textAlign:"left",
  marginBottom:vs(16)
 },
 footer: {
  position: 'absolute',
  flex: 1,
  left: 0,
  right: 0,
  bottom: 30,
  backgroundColor: COLORS.white,
  marginTop: vs(15),
 },
 button: {
  
 },
 buttomView: {
  flexDirection: "row",
  marginHorizontal:s(-15),
  marginLeft:s(30),
 },
 term: {
  fontSize: 12,
  lineHeight: vs(14),
  alignSelf: "center",
  marginLeft:s(-13)
 },
});
export default Privacy;