import React, { useState } from "react";
import { SafeAreaView, View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { s, vs, ms } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Headertext, Plantext, CustomButton, Hint, InputText } from "@components/index";
import {COLORS} from "@theme";
const Onboard = ({navigation}) => {
const [orgname, setOrgname] = useState("")
 return (
  <SafeAreaView style={styles.safeview}>
   <View style={{flexDirection:"row",justifyContent:"space-between"}}>
   <Icon name="angle-left" size={35} color={COLORS.backIcon} style={styles.icon} onPress={()=>navigation.goBack()}/>
   <TouchableOpacity
    onPress={()=>navigation.navigate("Handle")}
   >
      <Plantext value="Skip" style={{fontSize:ms(17), marginRight:s(10)}}/>
   </TouchableOpacity>
   </View>
   <View style={styles.headerText}>
    <Headertext
     value={"What’s your \ncurrent organization?"}
    />
    </View>
    <View style={styles.inputview}>
      <InputText
         placeholder="Organization Name"
         value={orgname}
         onChangeText={(value) => setOrgname(value)}
         />
    </View>
    <View style={styles.hintText}>
      <Hint
       value="We'll send you an SMS verification code"
      />
     </View>
     <CustomButton
      title="Continue"
      disabled={orgname.length > 0 ? false : true}
      onPress={()=>navigation.navigate("Position")}
     />
  </SafeAreaView>
 );
}
const styles = StyleSheet.create({
 safeview: {
  flex: 1,
  backgroundColor:COLORS.white,
 },
 icon:{
    marginLeft:s(10)
   },
   headerText:{
      marginTop: vs(50),
   },
   inputview: {
    flexDirection: "row",
    marginTop: vs(65),
    marginLeft: s(30),
    marginRight: s(30),
   },
   hintText:{
      marginTop:vs(-15),
      marginBottom:vs(100)
     },
});
export default Onboard;