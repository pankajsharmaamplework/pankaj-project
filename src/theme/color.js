const COLORS = {
        white: 'rgb(255,255,255)',
        black: 'rgb(0,0,0)',
        buttonColor: 'rgb(32,142,251)',
        buttonColorOpacity: 'rgba(32,142,251,0.5)',
        hintColor: 'rgb(168,168,168)',
        errColor: 'rgb(243,60,87)',
        backIcon: 'rgb(55,155,250)',
}
export default COLORS;
