import COLORS from "./color";
import FONTS from "./fonts";
import IMAGES from "./images";

export {COLORS, FONTS, IMAGES};