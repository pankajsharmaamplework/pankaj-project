export const FONTS = {
    'SFT-Regular': "SanFranciscoText-Regular",
    'SFT-Semibold': "SanFranciscoText-Semibold",
    'SFT-Medium': "SanFranciscoText-Medium",
}