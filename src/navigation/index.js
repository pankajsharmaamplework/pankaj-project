import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Welcome, Login, LoginEmail, VerifyCode, Username, Onboard, Position, Handle, Privacy } from "@screen";
const Stack = createNativeStackNavigator();

function App() {
 return (
  <NavigationContainer>
   <Stack.Navigator
    screenOptions={{headerShown:false}}
    initialRouteName="Welcome"
   >
    <Stack.Screen name="Welcome" component={Welcome}/>
    <Stack.Screen name="LoginWithPhone" component={Login}/>
    <Stack.Screen name="LoginWithEmail" component={LoginEmail}/>
    <Stack.Screen name="verification" component={VerifyCode}/>
    <Stack.Screen name="Username" component={Username}/>
    <Stack.Screen name="Onboard" component={Onboard}/>
    <Stack.Screen name="Position" component={Position}/>
    <Stack.Screen name="Handle" component={Handle}/>
    <Stack.Screen name="Privacy" component={Privacy}/>
   </Stack.Navigator>
  </NavigationContainer>
 );
}
export default App;