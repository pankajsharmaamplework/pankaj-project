import React from "react";
import {StyleSheet} from "react-native";
import { Input } from 'react-native-elements';
import { s, vs, ms } from 'react-native-size-matters';
const InputText = ({ placeholder, style,onChangeText,value }) => {
 return (
    <Input
        placeholder={placeholder}
        style={[styles.input,style]}
        onChangeText={onChangeText}
        value={value}
    />
 );
}
const styles = StyleSheet.create({
    input:{
        fontSize:ms(18),
        textAlign:"center",
    }
});
export default InputText;