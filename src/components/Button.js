import React from "react";
import { Button } from 'react-native-elements';
import { s, vs, ms } from 'react-native-size-matters';
import {COLORS} from "@theme";
const CustomButton = ({ title, disabled,onPress }) => {
 return (
  <Button
    title={title} 
    onPress={onPress}
    disabled={disabled} 
    disabledStyle={{ 
        backgroundColor: COLORS.buttonColorOpacity, 
    }} 
    disabledTitleStyle={{color: COLORS.white}}
    buttonStyle={{
        height:vs(40),
        marginLeft:s(45),
        marginRight:s(45)
    }}
  />
 );
}
export default CustomButton;