import React from "react";
import {Text, StyleSheet} from "react-native";
import { s, vs, ms} from "react-native-size-matters";
import {COLORS} from "@theme";
const Headertext = ({style, value}) => {
    return ( 
        <Text style={[styles.text, style]}>{value}</Text>
    );
}
const Plantext = ({style, value}) => {
    return (
        <Text style={[styles.plantext, style]}>{value}</Text>
    );
}
const Hint = ({style, value}) => {
    return (
        <Text style={[styles.hintText, style]}>{value}</Text>
    )
}
const styles = StyleSheet.create({
    text:{
        fontSize:ms(20),
        lineHeight:vs(24),
        letterSpacing:s(0.03),
        color:COLORS.black,
        textAlign:"center",
        fontFamily:"SanFranciscoText-Regular"
    },
    plantext:{
        fontSize:ms(14),
        lineHeight:vs(16),
        textAlign:"center",
        color:COLORS.buttonColor,
        fontFamily:"SanFranciscoText-Regular"
    },
    hintText:{
        fontSize:ms(12),
        lineHeight:vs(14),
        textAlign:"center",
        color:COLORS.hintColor,
        fontFamily:"SanFranciscoText-Regular"
    }
});
export { Headertext, Plantext, Hint};